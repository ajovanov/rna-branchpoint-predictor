class Metric:
    def __init__(self):
        self.tp = self.fp = self.tn = self.fn = 0
        self.multi_good = self.multi_bad = self.single_good = self.single_bad = 0
        
    def update(self, output, target):
        batch_size = target.shape[1]
        for i in range(batch_size):
            o = output[:, i].tolist()
            t = target[:, i].tolist()
            for j in range(len(o)):
                if o[j] == 0:
                    o = list(set(o[:j]))
                    break
            for j in range(len(t)):
                if t[j] == 0:
                    t = t[:j]
                    break

            if len(o) == 1 and len(t) == 1:
                self.single_good += 1
            elif len(o) == 1 and len(t) > 1:
                self.single_bad += 1
            elif len(o) > 1 and len(t) > 1:
                self.multi_good += 1
            else:
                self.multi_bad += 1
                
            for j in range(len(o)):
                if o[j] in t:
                    self.tp += 1
                else:
                    self.fp += 1
            for j in range(len(t)):
                if t[j] not in o:
                    self.fn += 1

    def precision(self):
        if self.tp + self.fp == 0:
            return -1;
        return self.tp / (self.tp + self.fp)

    def recall(self):
        if self.tp + self.fn == 0:
            return -1;
        return self.tp / (self.tp + self.fn)

    def accuracy(self):
        if self.tp + self.fp + self.tn + self.fn == 0:
            return -1;
        return (self.tp + self.tn) / (self.tp + self.fp + self.tn + self.fn)

    def __str__(self):
        return f'Precision = {self.precision()}\nRecall   = {self.recall()}\nAccuracy = {self.accuracy()}\nTP = {self.tp}\nTN = {self.tn}\nFP = {self.fp}\nFN = {self.fn}\n'    

class MetricPM2:
    def __init__(self):
        self.tp = self.fp = self.tn = self.fn = 0
        
    def update(self, output, target):
        batch_size = target.shape[1]
        for i in range(batch_size):
            o = output[:, i].tolist()
            t = target[:, i].tolist()
            for j in range(len(o)):
                if o[j] == 0:
                    o = list(set(o[:j]))
                    break
            for j in range(len(t)):
                if t[j] == 0:
                    t = t[:j]
                    break

            for j in range(len(o)):
                found = False
                for tt in t:
                    if o[j] in range(tt - 2, tt + 3):
                        self.tp += 1
                        found = True
                        break
                if not found:
                    self.fp += 1
                    
            for j in range(len(t)):
                found = False
                for oo in o:
                    if t[j] in range(oo - 2, oo + 3):
                        found = True
                        break
                if not found:
                    self.fn += 1

    def precision(self):
        if self.tp + self.fp == 0:
            return -1;
        return self.tp / (self.tp + self.fp)

    def recall(self):
        if self.tp + self.fn == 0:
            return -1;
        return self.tp / (self.tp + self.fn)

    def accuracy(self):
        if self.tp + self.fp + self.tn + self.fn == 0:
            return -1;
        return (self.tp + self.tn) / (self.tp + self.fp + self.tn + self.fn)

    def __str__(self):
        return f'Precision = {self.precision()}\nRecall   = {self.recall()}\nAccuracy = {self.accuracy()}\nTP = {self.tp}\nTN = {self.tn}\nFP = {self.fp}\nFN = {self.fn}\n'  
