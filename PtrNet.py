import torch
import torch.nn as nn

from data import RNASequence
from beam_search import BeamNode

class Encoder(nn.Module):
    def __init__(self, hidden_size):
        super().__init__()
        self.lstm = nn.LSTM(input_size=RNASequence.input_size, hidden_size=hidden_size, bidirectional=True)
        self.hidden_size = hidden_size

    def forward(self, seq):
        """ Forward call for PtrNet encoder
        :param seq: torch.tensor of dimension (seq_len, batch_size, RNASequence.input_size)
        :param seq_lens: list of length batch_size, contains lengths of unpadded sequences
        :returns: tuple (h_n, c_n), concatenated fwd/bkd hidden and cell states, and tensor *output* of all hidden states
        """                
        output, (h_n, c_n) = self.lstm(seq)
        h_n = torch.cat((h_n[0], h_n[1]), dim=1)
        c_n = torch.cat((c_n[0], c_n[1]), dim=1)
        return (h_n, c_n), output

    
class Pointer(nn.Module):
    def __init__(self, hidden_size):
        super().__init__()
        self.v = nn.Linear(hidden_size, 1, bias=False)
        self.W_e = nn.Linear(hidden_size, hidden_size, bias=False)
        self.W_d = nn.Linear(hidden_size, hidden_size, bias=False)
        self.hidden_size = hidden_size
        self.tanh = nn.Tanh()

    def forward(self, decoder_state, encoder_states):
        """ Forward call for Pointer
        :param decoder_state: last state from decoder
        :param encoder_states: tensor of all encoder states
        :returns: log-softmax scores for every input sequence
        """
        encoder_transform = self.W_e(encoder_states)
        decoder_transform = self.W_d(decoder_state)
        u = self.v(self.tanh(encoder_transform + decoder_transform)).squeeze(-1)
        return nn.functional.log_softmax(u, dim=0)


class PointerNetwork(nn.Module):
    def __init__(self, model_args):
        super().__init__()
        self.encoder_hidden_size = model_args['hidden_size']
        self.decoder_hidden_size = 2 * self.encoder_hidden_size
        self.teacher_forcing_ratio = model_args['teacher_forcing_ratio']
        self.emb = nn.Embedding.from_pretrained(torch.eye(5))
        self.encoder = Encoder(self.encoder_hidden_size)
        self.decoder_rnn = nn.LSTMCell(self.decoder_hidden_size, self.decoder_hidden_size)
        self.pointer = Pointer(self.decoder_hidden_size)

    def forward(self, seq, target=None, beam_width=None, alpha=None):
        seq = self.emb(seq)
        if beam_width is None:
            return self.greedy_decode(seq, target)
        else:
            return self.beam_search_decode(seq, target, beam_width, alpha)


    def greedy_decode(self, seq, target):
        batch_size = seq.shape[1]

        (h, c), encoder_states = self.encoder(seq)
        x = torch.zeros_like(h)

        pointer_log_scores = []
        pointer_indices = []

        use_teacher_forcing = torch.rand(1) <= self.teacher_forcing_ratio

        for i in range(target.shape[0] if target is not None else 10):
            h, c = self.decoder_rnn(x, (h, c))

            pointer_log_score = self.pointer(h, encoder_states) # (seq_len, batch_size)
            pointer_log_scores.append(pointer_log_score)

            pointer_index = torch.argmax(pointer_log_score, dim=0)
            pointer_indices.append(pointer_index)

            if use_teacher_forcing and target is not None:
                idx = torch.where(target[i] == -1, 0, target[i]).reshape(1, -1, 1).expand(1, -1, self.decoder_hidden_size)
            else:
                idx = pointer_index.reshape(1, -1, 1).expand(1, -1, self.decoder_hidden_size)
            x = torch.gather(encoder_states, dim=0, index=idx).squeeze(0)

        return torch.stack(pointer_indices, dim=0), torch.stack(pointer_log_scores, dim=0)

    def beam_search_decode(self, seq, target, beam_width, alpha):
        batch_size = seq.shape[1]
        best_nodes = []

        (h_encoder, c_encoder), encoder_states = self.encoder(seq)

        for batch_idx in range(batch_size):
            x = torch.zeros_like(h_encoder[batch_idx]).unsqueeze(0)
            h = h_encoder[batch_idx].unsqueeze(0)
            c = c_encoder[batch_idx].unsqueeze(0)
            
            node = BeamNode(1e9, x, h, c, None, None, torch.empty(0, device=seq.device))
            nodes = [node]
            finished_nodes = []
            
            for i in range(10):
                next_nodes = []
                for node in nodes:
                    x, h, c = node.state
                    h, c = self.decoder_rnn(x, (h, c))
                    pointer_log_score = self.pointer(h, encoder_states[:, batch_idx, :])
                    pointer_log_score = torch.topk(pointer_log_score, k=beam_width)
                    for j in range(beam_width):
                        pointer_index = pointer_log_score.indices[j]
                        log_score = pointer_log_score.values[j]
                        
                        x = encoder_states[pointer_index, batch_idx, :].unsqueeze(0)
                        next_log_score = log_score.reshape(-1) if len(node.log_ps) == 0 else torch.cat((node.log_ps, log_score.reshape(1)))
                        next_node = BeamNode(None, x, h, c, node, pointer_index, next_log_score)
                        next_node.score = -next_node.eval(alpha)
                        
                        if pointer_index == 0 and i == 0:
                            continue
                        if pointer_index == 0:
                            finished_nodes.append(next_node)
                        if pointer_index > 0:
                            next_nodes.append(next_node)
                nodes = sorted(next_nodes, key=lambda x : x.score)[:beam_width]
            best_nodes.append(sorted(finished_nodes, key=lambda x : x.score)[0])
        return best_nodes
