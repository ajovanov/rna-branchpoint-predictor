import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader

import pandas as pd
import numpy as np
import random
import argparse
import os
import csv
from tqdm import tqdm

import data
import PtrNet
import metric

def seed_worker(worker_id):
    """
    DataLoader worker init function to ensure reproducibility
    See https://pytorch.org/docs/stable/notes/randomness.html
    """
    worker_seed = torch.initial_seed() % 2**32
    np.random.seed(worker_seed)
    random.seed(worker_seed)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Pointer network for RNA branchpoint predictions.')
    parser.add_argument('--num-epochs', type=int, default=200, help='Number of epochs for training')
    parser.add_argument('--batch-size', type=int, default=256, help='training batch size')
    parser.add_argument('--hidden-size', type=int, default=128, help='LSTM hidden dimension size')
    parser.add_argument('--teacher-forcing-ratio', type=float, default=0.5, help='Teacher forcing ratio for learning')
    parser.add_argument('--wd', type=float, default=1e-5, help='Weight decay for Adam optimizer')
    parser.add_argument('--lr', type=float, default=1e-3, help='Learning rate for Adam optimizer')
    parser.add_argument('--train-data', type=str, default='pineda_balanced_train', help='Name of training data file (without csv extension)')
    parser.add_argument('--valid-data', type=str, default='pineda_balanced_valid', help='Name of validation data file (without csv extension)')
    parser.add_argument('--device', type=str, default='cpu', help='torch device')
    args = parser.parse_args()
    
    # Reproducibility seed
    torch.manual_seed(2001)
    g = torch.Generator()
    g.manual_seed(2001)
    
    device = torch.device(args.device)
    
    train_path = f'./data/{args.train_data}.csv'
    valid_path = f'./data/{args.valid_data}.csv'
    
    model_name = f'ne-{args.num_epochs}_bs-{args.batch_size}_hs-{args.hidden_size}_tfr-{args.teacher_forcing_ratio}_wd-{args.wd}_lr-{args.lr}_trained-on-{args.train_data}'
    model_path = f'./trained_models/{model_name}/'
    if not os.path.exists(model_path):
        os.makedirs(model_path)

    dataloader_params = {'batch_size': args.batch_size,
                         'shuffle': True,
                         'num_workers': 8,
                         'worker_init_fn': seed_worker,
                         'generator': g}
    
    training_set = data.RNADataset(rna_file=train_path)
    training_generator = DataLoader(training_set, **dataloader_params, collate_fn=data.collate_fn)
    
    validation_set = data.RNADataset(rna_file=valid_path)
    validation_generator = DataLoader(validation_set, **dataloader_params, collate_fn=data.collate_fn)

    model_args = {'hidden_size': args.hidden_size,
                  'teacher_forcing_ratio': args.teacher_forcing_ratio}
    rna_model = PtrNet.PointerNetwork(model_args).to(device)
    optimizer = optim.AdamW(rna_model.parameters(), weight_decay=args.wd, lr=args.lr)
    loss_fn = nn.CrossEntropyLoss(ignore_index=-1)

    best_precision = (-1, -1)
    best_recall = (-1, -1)
    patience = 20
    
    metrics = {'epoch': [],
               'training_precision': [],
               'training_recall': [],
               'training_loss': [],
               'validation_precision': [],
               'validation_recall': [],
               'validation_loss': []}
    
    for epoch in range(1, args.num_epochs + 1):
        print(f'Epoch {epoch}')

        if epoch - best_precision[1] > patience and epoch - best_recall[1] > patience:
            print(f'Early stopping after epoch {epoch - 1} with patience {patience}')
            break
        
        # TRAINING
        rna_model.train()
        met_training = metric.Metric()
        training_loss = 0
        for batch_idx, (seq, target) in tqdm(enumerate(training_generator)):
            seq, target = seq.to(device), target.to(device)
            optimizer.zero_grad()
            pointer_indices, pointer_log_scores = rna_model(seq, target)
            loss = loss_fn(pointer_log_scores.transpose(0, 2), target.transpose(0, 1))
            loss.backward()
            optimizer.step()

            met_training.update(pointer_indices, target)
            training_loss += loss.item()
            
        print(f'Training epoch loss: loss: {training_loss/len(training_generator):8.8f}')
        print(f'Training epoch precision: {met_training.precision()}')
        print(f'Training epoch recall: {met_training.recall()}')
        
        # VALIDATION
        rna_model.eval()
        met_validation = metric.Metric()
        validation_loss = 0
        for batch_idx, (seq, target) in tqdm(enumerate(validation_generator)):
            seq, target = seq.to(device), target.to(device)
            pointer_indices, pointer_log_scores = rna_model(seq, target)
            loss = loss_fn(pointer_log_scores.transpose(0, 2), target.transpose(0, 1))

            met_validation.update(pointer_indices, target)
            validation_loss += loss.item()
            
        print(f'Validation epoch loss: loss: {validation_loss/len(validation_generator):8.8f}')
        print(f'Validation epoch precision: {met_validation.precision()}')
        print(f'Validation epoch recall: {met_validation.recall()}')
        
        metrics['epoch'].append(epoch)
        metrics['training_precision'].append(met_training.precision())
        metrics['training_recall'].append(met_training.recall())
        metrics['training_loss'].append(training_loss / len(training_generator))
        metrics['validation_precision'].append(met_validation.precision())
        metrics['validation_recall'].append(met_validation.recall())
        metrics['validation_loss'].append(validation_loss / len(validation_generator))

        if met_validation.precision() > best_precision[0]:
            best_precision = (met_validation.precision(), epoch)
            torch.save(rna_model, os.path.join(model_path, f'model_best_precision.pt'))
        if met_validation.recall() > best_recall[0]:
            best_recall = (met_validation.recall(), epoch)
            torch.save(rna_model, os.path.join(model_path, f'model_best_recall.pt'))
            
    df = pd.DataFrame.from_dict(metrics)
    df.to_csv('./results/training_metrics.csv')
