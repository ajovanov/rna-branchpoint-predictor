from torch.utils.data import Dataset
import pandas as pd
import torch
import torch.nn as nn

class RNASequence:
    input_size = 5
    
def collate_fn(batch):
    """ 
    Collate function needed for variable-sized output padding in DataLoader
    Padding value is -1
    """
    seq, target = zip(*batch)
    seq = torch.stack(seq, dim=1)
    target = nn.utils.rnn.pad_sequence(target, padding_value=-1)
    return seq, target

class RNADataset(Dataset):
    def __init__(self, rna_file):
        self.rna_csv = pd.read_csv(rna_file)
        self.d = dict([(y, x) for x, y in enumerate('0ACGT')])

        self.sequences = [torch.tensor(list(map(lambda x : self.d[x], '0' + self.rna_csv['seq'][i]))) for i in range(len(self.rna_csv))]
        self.targets = [torch.tensor(list(map(lambda x : int(x) + 1, self.rna_csv['bp_position'][i].split(','))) + [0], dtype=torch.long) for i in range(len(self.rna_csv))]
        
    def __len__(self):
        return len(self.rna_csv)

    def __getitem__(self, idx):
        return self.sequences[idx], self.targets[idx]
